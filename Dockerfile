FROM debian:stable-slim

RUN apt-get update && apt-get dist-upgrade -y

RUN apt install -y prometheus-blackbox-exporter ca-certificates

CMD ["prometheus-blackbox-exporter","--config.file=/etc/prometheus/blackbox.yml","--web.listen-address=0.0.0.0:9115"]
