# Blackbox Exporter

The blackbox exporter allows blackbox probing of endpoints over HTTP, HTTPS, DNS, TCP, ICMP and gRPC.

## Install

Define on docker-compose.yml

```
...
services:
  blackbox:
    image: registry.commonscloud.coop/blackbox
    user: "1001:1001"
    restart: always
    networks:
      - exporters-net
...

```

## Configure

Define on prometheus.yml

```
  - job_name: 'blackbox'
    scrape_interval: 300s
    metrics_path: /probe
    params:
      module: [http_2xx]
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: blackbox:9115
    static_configs:
      - targets:
        - https://femprocomuns.coop
	...
```

## Visualize

On Grafana, import a Blackbox Exporter Dashboard like:

https://grafana.com/grafana/dashboards/7587-prometheus-blackbox-exporter/
